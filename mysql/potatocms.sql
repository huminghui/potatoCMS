/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50553
 Source Host           : localhost:3306
 Source Schema         : potatocms

 Target Server Type    : MySQL
 Target Server Version : 50553
 File Encoding         : 65001

 Date: 09/05/2020 16:49:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for po_article_content
-- ----------------------------
DROP TABLE IF EXISTS `po_article_content`;
CREATE TABLE `po_article_content`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `column_id` int(10) NULL DEFAULT NULL COMMENT '栏目id',
  `author` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '作者',
  `property` char(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'h头条c推荐',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图片路径',
  `seo_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'seo标题',
  `seo_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'seo关键字',
  `seo_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'seo描述',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容详情',
  `is_publish` int(2) NULL DEFAULT NULL COMMENT '1发布2草稿',
  `browse_volume` int(10) NULL DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '列表页内容（产品和新闻等）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_article_content
-- ----------------------------
INSERT INTO `po_article_content` VALUES (5, '年轻人是机遇，也是B站的挑战', 93, '半佛仙人', 'h,c', '/potatocms/public/uploads/20200426\\7b5763312518dea7e5e80ce4c23486d7.jpg', 'B站很火，真的很火。', 'B站很火，真的很火。', 'B站很火，真的很火。', '<p>B站很火，真的很火。</p><p>但如果跳出B站，看整个互联网世界，最近B站的声誉又可以说是有点四面楚歌，到处都在传播着各种B站UP的负面信息的新闻，好像各路坏蛋都改行来做UP主了。</p><p>作为一个多年B站用户，看着B站正在面临机遇和挑战，总该说点什么。</p><p>因为这代表B站在破圈，在成长，在成熟的关键阶段。</p><p>成长必然是有阵痛的，非议是破圈的代价，非议越大，熬过去之后的收获就会越大。</p><p>只要熬过去，就是真正的成人网站了。</p><p>要当年腾讯彻底成熟的标志不就是3Q大战么。</p><p>不被社会毒打，怎么能成熟呢？第一次比较痛，之后就好了。</p><p>先说机遇，再说挑战。</p><p>机遇一，年轻的可怕的用户群体。</p><p>尽管亏损，但是B站的未来依然不可限量。</p><p>2019财报显示，B站13亿的亏损背后，是B站的月均活跃用户达到了1.3亿，同比增长40%，日均活跃用户3800万，同比增长41%，通过考试的正式会员数6800万，同比增长50%，并且新用户第12个月留存率为80%+。</p><p>简单理解这组数字，就是每天至少有3800万人打开B站，平均冲浪77分钟，将近一个半小时，真的持久。</p><p>这些人是谁？是年轻人。</p><p>有多年轻？B站平均用户年龄是，21岁。</p><p>而B站平均新注册用户的年龄不到20岁。</p><p>并且，这些人又猛又持久，一天能玩儿77分钟。</p><p>同时依据留存率可以得出，他们一旦成为B站正式会员，12个月后80%以上的人都会留下来，并不是只蹭蹭不进来。</p><p>这个活跃度，用户年龄结构以及用户粘性在当前的中国互联网界高到违反广告法了。</p><p>这些背后代表什么？</p><p>代表未来社会的中流砥柱，以及现在社会的主力年轻人，都在B站，或者说，都高速在涌入B站，并且来了就不走。</p><p>上一个这么霸气的产品，叫做QQ。</p><p>今天企鹅帝国的一切，都是基于QQ。</p><p>所以，为什么各种官媒都在入驻B站？</p><p>所以，为什么腾讯拼命在投资B站并且成为最大机构股东？</p><p>所以，为什么阿里不断在买入B站股份并且疯狂冠名B站晚会？</p><p>所以，为什么索尼近期简单粗暴的直接砸钱投B站？</p><p>所以为什么明明B站不断在亏损，但是大家都来了？</p><p>因为中国的年轻人，都在这里。</p><p>谁抓住年轻人，谁就有未来。</p><p>第二个优势，年轻人对B站有归属感，并且乐意付费。</p><p>光把人弄进来不行，还要千方百计得到他们的心，B站独有的生态和社区归属感，牢牢抓住了年轻人的心。</p><p>B站最吸引人的两个点，1个是相对自由但不失底线的社区氛围，B站鼓励用户发言，但B站氛围同时也在克制用户的无底线发言。</p><p>另一个是独有的弹幕文化，很多视频有弹幕和没有弹幕真的是2个视频。</p><p>更重要的是，弹幕把年轻人的孤独感聚集在一起，很多年轻人在网络上嬉笑怒骂，但是在现实生活中是孤独的。</p><p>他们很难找到一个可以展现内心真实想法、毫无顾忌地倾吐烦恼的地方。</p><p>而B站的弹幕，给了他们机会，他们通过弹幕，找到了热闹和快乐，大家一切开开玩笑，玩玩儿梗，这是高粘性的重要因素，我自己就特别喜欢看自己视频的弹幕，很多骚弹幕看得我合不拢腿。</p><p>另外弹幕背后，还暗含了另一层社交。</p><p>不同人的世界通过弹幕开始交集，有时候用户在某个老视频中看到的一条弹幕，可能是五六年前发的，而发这条弹幕的人，或许再也不会回来看这个视频。</p><p>不同时空的人，在同一个视频下，用弹幕进行了交流，所有视频的观看者，又成了这场交流的见证者和参与者。</p><p>世界与时间的分界线，在这一刻被打破，弹幕中是用户的人生交集。</p><p>一瞬间，也是一生。</p><p>这种感觉十分微妙，但真的很棒。</p><p>这种归属感和孤独消解带来的核心优势就是，年轻人乐意付费。</p><p>虽然嘴上都是白嫖快乐和下次一定，但是从付费大会员的人数暴涨上，能看到大家真的愿意掏钱，很多人大会员一买就是好几年。</p><p>这背后的原因是因为在这些年轻人还小的时候，B站就和他们站在一起，他们把B站当家。</p><p>在潜在用户还没有能力的时候陪伴他们成长，消除他们的孤独，当他们成熟后，在B站消费不叫消费，叫补贴家里。</p><p>他们是最优质，最有消费能力，最有消费意愿的用户，是B站最宝贵的伙伴与财富。</p><p>他们是B站的基石。</p><p>讲完机遇讲挑战，实际上机遇和挑战是一体的，很多挑战就是由机遇衍生出来的。</p><p>毕竟这个世界没有只有好处没有坏处的事情。</p><p><br/></p>', 0, 0, '2020-04-26 16:06:50', '2020-04-28 14:48:55', NULL);
INSERT INTO `po_article_content` VALUES (6, '解读拼多多黄峥，那封充满哲学和诗意的股东信', 92, 'geekerdeng', 'h,c', '/potatocms/public/uploads/20200426\\64117e34c7e32b311b1143d8b8e00ea5.jpg', '不要与时间为敌：历史上存在的，今天不一定还合理；现在合理的，未来不一定还合理。', '不要与时间为敌：历史上存在的，今天不一定还合理；现在合理的，未来不一定还合理。', '不要与时间为敌：历史上存在的，今天不一定还合理；现在合理的，未来不一定还合理。', '<p>4 月 25 日凌晨，拼多多发布 2019 财年年报，披露财年各项完整运营数据。年报显示，2019 年拼多多实现成交额 10066 亿元，平台年活跃买家数达 5.852 亿，年营收 301.4 亿元。从用户数看，创立 4 年半的拼多多已经成为中国第二大电商平台。</p><p>拼多多创始人、董事长兼 CEO 黄峥随同年报发布了股东信，这是 2018 年以来黄峥连续发布的第三封年度致股东信。</p><p>黄峥这次的股东信却没有从业务实际出发，而是结合疫情对大众生活的影响，对「时间」这个宏大命题谈了一些他的看法。这封充满深奥大词和数学物理公式的文章让人感觉不知所云，但放在年报语境中，黄峥显然会在其中表达它对拼多多的过去、现在和未来的看法。</p><p><br/></p>', 0, 2, '2020-04-26 16:15:27', '2020-04-28 15:21:03', NULL);

-- ----------------------------
-- Table structure for po_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `po_auth_group`;
CREATE TABLE `po_auth_group`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `rules` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_auth_group
-- ----------------------------
INSERT INTO `po_auth_group` VALUES (9, '内容管理', 1, '17,18,19,20,21,22', '2020-04-27 10:51:02', NULL, NULL);
INSERT INTO `po_auth_group` VALUES (8, '管理员组', 1, '13,16,15,14,17,35,39,40,41,36,42,43,44,37,45,38,18,19,20,21,22,46,23,24,25,26,27,28,29,30,31,32,33,34', '2020-04-27 10:50:08', '2020-04-28 10:40:35', NULL);

-- ----------------------------
-- Table structure for po_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `po_auth_group_access`;
CREATE TABLE `po_auth_group_access`  (
  `uid` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE,
  INDEX `uid`(`uid`) USING BTREE,
  INDEX `group_id`(`group_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of po_auth_group_access
-- ----------------------------
INSERT INTO `po_auth_group_access` VALUES (6, 8);

-- ----------------------------
-- Table structure for po_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `po_auth_rule`;
CREATE TABLE `po_auth_rule`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `title` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT 1,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `pid` int(11) NULL DEFAULT NULL COMMENT '父id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '新增时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 47 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_auth_rule
-- ----------------------------
INSERT INTO `po_auth_rule` VALUES (16, 'column/del', '栏目删除', 1, 1, '', 13, '2020-04-26 17:21:45', '2020-04-27 10:35:13', NULL);
INSERT INTO `po_auth_rule` VALUES (15, 'column/edit', '栏目修改', 1, 1, '', 13, '2020-04-26 17:21:00', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (14, 'column/add', '栏目添加', 1, 1, '', 13, '2020-04-26 17:20:32', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (13, 'column/lst', '栏目管理', 1, 1, '', 0, '2020-04-26 17:19:55', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (17, 'content/lst', '内容管理', 1, 1, '', 0, '2020-04-27 10:25:52', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (18, 'banner/lst', '轮播图管理', 1, 1, '', 0, '2020-04-27 10:32:13', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (19, 'banner/add', '轮播图添加', 1, 1, '', 18, '2020-04-27 10:32:44', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (20, 'banner/edit', '轮播图修改', 1, 1, '', 18, '2020-04-27 10:33:56', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (21, 'banner/del', '轮播图删除', 1, 1, '', 18, '2020-04-27 10:35:43', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (22, 'setting/index', '系统管理', 1, 1, '', 0, '2020-04-27 10:36:31', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (23, 'authRule/lst', '权限管理', 1, 1, '', 0, '2020-04-27 10:39:04', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (24, 'authRule/add', '权限增加', 1, 1, '', 23, '2020-04-27 10:39:56', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (25, 'authRule/edit', '权限修改', 1, 1, '', 23, '2020-04-27 10:40:22', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (26, 'authRule/del', '权限删除', 1, 1, '', 23, '2020-04-27 10:40:53', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (27, 'user/lst', '用户列表', 1, 1, '', 0, '2020-04-27 10:41:44', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (28, 'user/add', '用户添加', 1, 1, '', 27, '2020-04-27 10:42:10', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (29, 'user/edit', '用户修改', 1, 1, '', 27, '2020-04-27 10:42:28', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (30, 'user/del', '用户删除', 1, 1, '', 27, '2020-04-27 10:42:50', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (31, 'authGroup/lst', '用户组列表', 1, 1, '', 0, '2020-04-27 10:44:55', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (32, 'authGroup/add', '用户组添加', 1, 1, '', 31, '2020-04-27 10:45:18', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (33, 'authGroup/edit', '用户组修改', 1, 1, '', 31, '2020-04-27 10:48:57', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (34, 'authGroup/del', '用户组删除', 1, 1, '', 31, '2020-04-27 10:49:29', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (35, 'content/productlist', '产品模板管理', 1, 1, '', 17, '2020-04-27 11:36:00', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (36, 'content/articlelist', '文章模板管理', 1, 1, '', 17, '2020-04-27 11:36:37', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (37, 'content/messagelist', '留言模板管理', 1, 1, '', 17, '2020-04-27 11:37:11', '2020-04-27 15:17:18', NULL);
INSERT INTO `po_auth_rule` VALUES (38, 'content/singlepage', '单页模型管理', 1, 1, '', 17, '2020-04-27 14:41:45', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (39, 'content/addproduct', '产品增加', 1, 1, '', 35, '2020-04-27 14:48:43', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (40, 'content/editproduct', '产品修改', 1, 1, '', 35, '2020-04-27 14:49:35', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (41, 'content/deleteproduct', '产品删除', 1, 1, '', 35, '2020-04-27 14:56:42', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (42, 'content/addarticle', '文章增加', 1, 1, '', 36, '2020-04-27 14:58:28', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (43, 'content/editarticle', '文章修改', 1, 1, '', 36, '2020-04-27 14:59:07', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (44, 'content/deletearticle', '文章删除', 1, 1, '', 36, '2020-04-27 14:59:42', '2020-04-27 15:00:40', NULL);
INSERT INTO `po_auth_rule` VALUES (45, 'content/deletemessage', '留言删除', 1, 1, '', 37, '2020-04-27 15:18:24', NULL, NULL);
INSERT INTO `po_auth_rule` VALUES (46, 'setting/add', '系统内容添加', 1, 1, '', 22, '2020-04-28 10:40:19', NULL, NULL);

-- ----------------------------
-- Table structure for po_banner
-- ----------------------------
DROP TABLE IF EXISTS `po_banner`;
CREATE TABLE `po_banner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pc_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '电脑端图片',
  `m_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '手机端图片',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接地址',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `is_hidden` tinyint(1) NULL DEFAULT NULL COMMENT '0=发布1=隐藏',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_banner
-- ----------------------------
INSERT INTO `po_banner` VALUES (5, '/potatocms/public/uploads/20200427\\0b10cf4aed05c5cc4acce83dab390157.jpg', '/potatocms/public/uploads/20200427\\56d396910de9c0fcf080a5cf7a803672.jpg', '高端品质，匠心制作', '', '2020-04-27 10:30:46', NULL, NULL, 0);

-- ----------------------------
-- Table structure for po_column
-- ----------------------------
DROP TABLE IF EXISTS `po_column`;
CREATE TABLE `po_column`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '栏目ID',
  `parent_id` int(10) NULL DEFAULT NULL COMMENT '栏目上级ID',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '栏目名称',
  `m_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '栏目图片手机端',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '栏目图片',
  `template_type` tinyint(1) NULL DEFAULT NULL COMMENT '栏目模板类型：0：单页模型1：文章模型2：产品模型3：留言模型',
  `seo_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'seo标题',
  `seo_keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'seo关键字',
  `seo_description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'seo描述',
  `sort_order` int(10) NULL DEFAULT 1 COMMENT '栏目排序',
  `is_hidden` tinyint(1) NULL DEFAULT 0 COMMENT '是否隐藏0：隐藏1：显示',
  `create_time` datetime NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `template_page` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '模板类型',
  `mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 102 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统栏目' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_column
-- ----------------------------
INSERT INTO `po_column` VALUES (91, 0, '新闻动态', '/potatocms/public/uploads/20200508\\7c2744163ed8e10d0240208a5813ef04.jpg', '/potatocms/public/uploads/20200508\\1e561f862e330e3b789cc1f300a9af05.jpg', 1, '新闻动态SEO标题', '新闻动态SEO关键字', '新闻动态SEO描述', 3, 0, '2020-04-26 15:42:43', '2020-05-08 09:17:52', NULL, NULL);
INSERT INTO `po_column` VALUES (89, 86, '精细筛分系列', '/potatocms/public/uploads/20200507\\64c38705876d6e8c59be2bd553f85037.jpg', '/potatocms/public/uploads/20200507\\7cee879e438993133255a979da2611a8.jpg', 2, '精细筛分系列SEO标题', '精细筛分系列SEO关键字', '精细筛分系列SEO描述', 1, 0, '2020-04-26 15:41:06', '2020-05-07 16:44:14', NULL, NULL);
INSERT INTO `po_column` VALUES (87, 86, '振动筛分系列', '/potatocms/public/uploads/20200507\\b8c20c218e0752ae1677b6faea167e06.jpg', '/potatocms/public/uploads/20200507\\e1d95ec52368f16b6652f4db02c51331.jpg', 2, '振动筛分系列SEO标题', '振动筛分系列SEO关键字', '振动筛分系列SEO描述', 1, 0, '2020-04-26 15:39:08', '2020-05-07 16:44:29', NULL, NULL);
INSERT INTO `po_column` VALUES (90, 0, '案例中心', '/potatocms/public/uploads/20200508\\fd259f720e1771fda8110d60f11cae62.jpg', '/potatocms/public/uploads/20200508\\fb9153dc0ebcefdfebbfe5204302d814.jpg', 2, '', '', '', 4, 0, '2020-04-26 15:41:43', '2020-05-08 09:18:32', NULL, NULL);
INSERT INTO `po_column` VALUES (88, 86, '振动给料系列', '/potatocms/public/uploads/20200507\\2c312421495533c1d6b974187cf0319c.jpg', '/potatocms/public/uploads/20200507\\1a8a306e0756531d5040fbf24741f225.jpg', 2, '振动给料系列SEO标题', '振动给料系列SEO关键字', '振动给料系列SEO描述', 1, 0, '2020-04-26 15:40:23', '2020-05-07 16:44:44', NULL, NULL);
INSERT INTO `po_column` VALUES (86, 0, '产品中心', '/potatocms/public/uploads/20200507\\55ad2eaa525d78d607a3764fbcab384b.jpg', '/potatocms/public/uploads/20200507\\bdab15b5243b3ea17e13599721e1ce04.jpg', 2, '产品中心SEO标题', '产品中心SEO关键字', '产品中心SEO描述', 2, 0, '2020-04-26 15:38:15', '2020-05-07 16:44:00', NULL, NULL);
INSERT INTO `po_column` VALUES (92, 91, '公司新闻', '/potatocms/public/uploads/20200508\\3a4efa2c7da819b542d6b8a5487eb6e6.jpg', '/potatocms/public/uploads/20200508\\51924f7d130c3c5fec73d3f9f92fb26f.jpg', 1, '公司新闻SEO标题', '公司新闻SEO关键字', '公司新闻SEO描述', 1, 0, '2020-04-26 15:43:34', '2020-05-08 09:18:06', NULL, NULL);
INSERT INTO `po_column` VALUES (93, 91, '行业新闻', '/potatocms/public/uploads/20200508\\d4c0ef11f3cec0362aa891b25b86cb85.jpg', '/potatocms/public/uploads/20200508\\de00c6310b1883f95488329d179d11c9.jpg', 1, '行业新闻SEO标题', '行业新闻SEO关键字', '行业新闻SEO描述', 1, 0, '2020-04-26 15:44:16', '2020-05-08 09:18:19', NULL, NULL);
INSERT INTO `po_column` VALUES (94, 0, '关于我们', '/potatocms/public/uploads/20200430\\d062d640f4877917d1e4636914aaedb5.jpg', '/potatocms/public/uploads/20200430\\9d539eef397b57bd68ef4920723128b5.jpg', 0, '关于我们SEO标题', '关于我们SEO关键字', '关于我们SEO描述', 1, 0, '2020-04-26 15:44:57', '2020-04-30 11:15:21', NULL, NULL);
INSERT INTO `po_column` VALUES (95, 94, '公司简介', '/potatocms/public/uploads/20200430\\2f2ace8983704b37aeb9cb4c05756f32.jpg', '/potatocms/public/uploads/20200430\\c2fbccd6903ed70da664bbe91597c0f5.jpg', 0, '公司简介SEO标题', '公司简介SEO关键字', '公司简介SEO描述', 1, 0, '2020-04-26 15:45:46', '2020-04-30 15:30:52', NULL, NULL);
INSERT INTO `po_column` VALUES (96, 94, '企业文化', '/potatocms/public/uploads/20200430\\1d3f6911004d4b15bf43e4906dfd593e.jpg', '/potatocms/public/uploads/20200430\\87f5a0df3d1629f576ad965ead0af8a7.jpg', 0, '企业文化', '企业文化', '企业文化', 1, 0, '2020-04-26 15:46:13', '2020-04-30 15:31:06', NULL, NULL);
INSERT INTO `po_column` VALUES (97, 0, '人力资源', '/potatocms/public/uploads/20200508\\3b8d17eef94d95851c99b2dd3874e9d3.jpg', '/potatocms/public/uploads/20200508\\ea88cef973484e1aa30fdfedb8ffff7b.jpg', 1, '人力资源', '人力资源', '人力资源', 5, 0, '2020-04-26 15:47:20', '2020-05-08 09:18:54', NULL, NULL);
INSERT INTO `po_column` VALUES (98, 97, '招聘岗位', '/potatocms/public/uploads/20200508\\fcccf03fd9ba7cf89c88f83abc149b1d.jpg', '/potatocms/public/uploads/20200508\\ad1b7bde4c0fd673ef92012e45eba944.jpg', 1, '招聘岗位', '招聘岗位', '招聘岗位', 1, 0, '2020-04-26 15:48:08', '2020-05-08 09:19:16', NULL, NULL);
INSERT INTO `po_column` VALUES (99, 97, '加入我们', '/potatocms/public/uploads/20200508\\852a1a2e72069f060792248c7786bdac.jpg', '/potatocms/public/uploads/20200508\\a54963e92c56dd070c308a10069f1ed5.jpg', 3, '加入我们', '加入我们', '加入我们', 1, 0, '2020-04-26 15:48:41', '2020-05-08 09:19:29', NULL, NULL);
INSERT INTO `po_column` VALUES (100, 0, '联系我们', '/potatocms/public/uploads/20200508\\f50de04d56aaacf4c64ef793de1f3fa2.jpg', '/potatocms/public/uploads/20200508\\dc7d6768baca258d73d95d895d25c198.jpg', 0, '联系我们', '联系我们', '联系我们', 6, 0, '2020-04-26 15:49:17', '2020-05-08 09:19:42', NULL, NULL);
INSERT INTO `po_column` VALUES (101, 100, '在线留言', '/potatocms/public/uploads/20200508\\7291813f478e2d013915c1eed8aca868.jpg', '/potatocms/public/uploads/20200508\\3b4230602e6b7acb990a1d9c61e3acb3.jpg', 3, '在线留言', '在线留言', '在线留言', 1, 0, '2020-04-26 15:49:47', '2020-05-08 09:19:54', NULL, NULL);

-- ----------------------------
-- Table structure for po_message_content
-- ----------------------------
DROP TABLE IF EXISTS `po_message_content`;
CREATE TABLE `po_message_content`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `column_id` int(10) NULL DEFAULT NULL COMMENT '栏目id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `telephone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `create_time` datetime NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_message_content
-- ----------------------------
INSERT INTO `po_message_content` VALUES (10, 101, '是是是', 'dd', 'dd\r\n                            ', '2020-05-08 11:37:28', NULL, NULL);
INSERT INTO `po_message_content` VALUES (11, 99, 'jiaruwom', '15037198339', '1222\r\n                            ', '2020-05-08 11:42:57', NULL, NULL);

-- ----------------------------
-- Table structure for po_product_content
-- ----------------------------
DROP TABLE IF EXISTS `po_product_content`;
CREATE TABLE `po_product_content`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `column_id` int(10) NULL DEFAULT NULL COMMENT '栏目id',
  `author` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '作者',
  `property` char(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'h头条c推荐',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图片路径',
  `seo_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'seo标题',
  `seo_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'seo关键字',
  `seo_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'seo描述',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容详情',
  `is_publish` int(2) NULL DEFAULT NULL COMMENT '1发布2草稿',
  `browse_volume` int(10) NULL DEFAULT 0 COMMENT '浏览量',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '列表页内容（产品和新闻等）' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_product_content
-- ----------------------------
INSERT INTO `po_product_content` VALUES (31, '产品一', 89, 'h', 'c', '/potatocms/public/uploads/20200428\\d3f3554e3cd518e08d0dc4883dd25652.jpg', '产品一', '产品一', '产品一', '<p>产品一</p>', 0, 0, '2020-04-28 13:41:54', NULL, NULL);
INSERT INTO `po_product_content` VALUES (32, '产品二', 87, 'hu', 'c', '/potatocms/public/uploads/20200428\\228964b267e8693d02838fdb4aa358ef.jpg', '产品二', '产品二', '产品二', '<p>产品二</p>', 0, 0, '2020-04-28 13:45:45', NULL, NULL);
INSERT INTO `po_product_content` VALUES (33, '产品三', 88, 'hu', 'c', '/potatocms/public/uploads/20200428\\f10d5411fbf466712596b75c1ce3b4a1.jpg', 'sss', '产品三', '产品三', '<p>产品三</p>', 0, 0, '2020-04-28 13:47:16', NULL, NULL);
INSERT INTO `po_product_content` VALUES (34, '产品四', 89, 'hu', 'c', '/potatocms/public/uploads/20200428\\8948286d86986341650db983967f2466.jpg', '产品四', '产品四', '产品四', '<p>产品四</p>', 0, 0, '2020-04-28 13:48:16', NULL, NULL);
INSERT INTO `po_product_content` VALUES (35, '产品五', 89, 'hu', 'c', '/potatocms/public/uploads/20200428\\38e6babdf37d6801bd17e9d045ef553a.jpg', '产品五', '产品五', '产品五', '<p>产品五</p>', 0, 0, '2020-04-28 13:48:43', NULL, NULL);
INSERT INTO `po_product_content` VALUES (36, '产品六', 89, 'hu', 'c', '/potatocms/public/uploads/20200428\\1821903a62c1abc4689b97e7f83af9fc.jpg', '产品六', '产品六', '产品六', '<p>产品六</p>', 0, 0, '2020-04-28 13:49:22', NULL, NULL);
INSERT INTO `po_product_content` VALUES (37, '产品七', 89, 'hu', 'c', '/potatocms/public/uploads/20200428\\db53524938b0c67594ac96c911220bf4.jpg', '产品七', '产品七', '产品七', '<p>产品七</p>', 0, 2, '2020-04-28 13:50:12', NULL, NULL);
INSERT INTO `po_product_content` VALUES (38, '产品八', 89, 'hu', 'c', '/potatocms/public/uploads/20200428\\877c8758d346a77120a24cf57b853488.jpg', '产品八', '产品八', '产品八', '<p>产品八</p>', 0, 0, '2020-04-28 13:50:51', NULL, NULL);
INSERT INTO `po_product_content` VALUES (39, '产品九', 89, 'hu', NULL, '/potatocms/public/uploads/20200428\\0d7d98d8da79e18d80dc273cefcdb11a.jpg', '产品九', '产品九', '产品九', '<p>产品九</p>', 0, 15, '2020-04-28 13:51:30', '2020-04-28 14:39:25', NULL);

-- ----------------------------
-- Table structure for po_setting
-- ----------------------------
DROP TABLE IF EXISTS `po_setting`;
CREATE TABLE `po_setting`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `website_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '网站名称',
  `logo_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'logo图片',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '关键词',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `telephone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '电话',
  `record` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备案',
  `copyright` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '版权信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '增加时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_setting
-- ----------------------------
INSERT INTO `po_setting` VALUES (2, 'potatoCMS最优秀CMS', '/potatocms/public/uploads/20200428\\17937f5b6a5fe6c03cc687fec949c26f.png', 'potatoCMS最优秀CMS', 'potatoCMS，优秀', 'potatoCMS', '15037198339', '豫icp：66668888', 'Copyright © 2019-2039 Potato Studio. All rights reserved', '2020-04-28 10:46:45', '2020-04-28 11:08:34', NULL);

-- ----------------------------
-- Table structure for po_single_content
-- ----------------------------
DROP TABLE IF EXISTS `po_single_content`;
CREATE TABLE `po_single_content`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `column_id` int(10) NULL DEFAULT NULL COMMENT '栏目id',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '内容',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '单页面内容' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_single_content
-- ----------------------------
INSERT INTO `po_single_content` VALUES (9, 95, '<p>突如其来的“世纪疫情”正在全球范围蔓延，其影响之深远足以改变世界运行规则。所人都是抗击疫情的亲历者，都有自己的疫情记忆，都将在疫情后反思成长向前。网友说“每天都在见证历史”，人类与新冠病毒的搏斗注定被载入史册，人们的记忆会成为这段历史的一部分。正是因为此，国家图书馆发起的“中国战‘疫’”记忆库建设项目，显得意义非凡。</p>', '2020-04-26 16:21:52', NULL);
INSERT INTO `po_single_content` VALUES (10, 96, '<p>国务院联防联控机制定于4月25日15时在北京国二招宾馆（北京市西直门南大街6号）东楼三层中会议厅召开新闻发布会，介绍电子商务促进消费和助力经济提质升级工作情况，商务部、工业和信息化部、国家邮政局、供销总社有关司局负责人回答媒体提问。</p><p>商务部电子商务司副司长蔡裕东先生、工业和信息化部信息技术发展司一级巡视员李颖女士、供销合作总社合作指导部部长刘进喜先生、国家邮政局市场监管司副司长边作栋先生，请他们就推动网络消费、工业电子商务、发展农村电商、无人配送管理等来回答媒体提问。<br/></p><p>大家对电子商务这个词都非常熟悉，但工业电子商务还属于比较专业的范畴。通俗的讲，消费电子商务主要面向最终消费者，工业电子商务更多是企业间的，是数字化的供应链。近年来，随着互联网加速从消费环节、虚拟领域向生产环节、实体经济领域延伸，工业电子商务加速成长。特别是工业互联网的快速发展，成为新一代电子商务发展的重要基础和支撑。<br/></p><p>近年来，工信部遴选了41个工业电商的试点示范项目，持续推进工业电子商务创新发展，截至2020年3月底，我国重点行业骨干企业的工业电商普及率达到62.5%。在原材料、装备等领域培育了一批交易规模达到百亿级、千亿级的工业电子商务平台。疫情发生以来，工业电子商务企业聚焦防疫复工难点、痛点、堵点问题，充分发挥支撑和赋能的作用。<br/></p><p><br/></p>', '2020-04-26 16:22:56', NULL);
INSERT INTO `po_single_content` VALUES (11, 94, '<p>关于我们</p>', '2020-04-26 16:23:34', NULL);

-- ----------------------------
-- Table structure for po_user
-- ----------------------------
DROP TABLE IF EXISTS `po_user`;
CREATE TABLE `po_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of po_user
-- ----------------------------
INSERT INTO `po_user` VALUES (6, 'admin', '202cb962ac59075b964b07152d234b70', '2020-04-27 11:21:25', '2020-04-27 11:21:34', NULL);

SET FOREIGN_KEY_CHECKS = 1;
