<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:73:"D:\phpS\PHPTutorial\WWW\potatocms/application/index\view\index\index.html";i:1588231261;s:73:"D:\phpS\PHPTutorial\WWW\potatocms\application\index\view\public\head.html";i:1588231261;s:73:"D:\phpS\PHPTutorial\WWW\potatocms\application\index\view\public\foot.html";i:1588045237;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $setting['website_name']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="keywords" content="<?php echo $setting['keywords']; ?>" />
    <meta name="description" content="<?php echo $setting['description']; ?>" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" >
    <link rel="stylesheet" href="/potatocms/public/static/index/common/website.css" >
    <script src="https://cdn.bootcss.com/jquery/2.2.2/jquery.js"></script>
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bsnavbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo url('/index'); ?>"><img src="<?php echo $setting['logo_url']; ?>" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bsnavbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<?php echo url('/index'); ?>">网站首页</a></li>
                <?php if(is_array($columnName) || $columnName instanceof \think\Collection || $columnName instanceof \think\Paginator): $i = 0; $__LIST__ = $columnName;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <li class="dropdown">
                    <a href=" <?php if($v['template_type'] == 0): ?><?php echo url('/page',array('id'=>$v['id'])); elseif($v['template_type'] == 1): ?><?php echo url('/article',array('id'=>$v['id'])); elseif($v['template_type'] == 2): ?><?php echo url('/product',array('id'=>$v['id'])); else: ?><?php echo url('/message',array('id'=>$v['id'])); endif; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $v['column_name']; ?><span class="caret"></span></a>
                    <?php if($v['children'] != 0): ?>
                    <ul class="dropdown-menu" >
                        <?php if(is_array($v['children']) || $v['children'] instanceof \think\Collection || $v['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $v['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$son): $mod = ($i % 2 );++$i;if($son['template_type'] == 0): ?>
                        <li><a href="<?php echo url('/page',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php elseif($son['template_type'] == 1): ?>
                        <li><a href="<?php echo url('/article',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php elseif($son['template_type'] == 2): ?>
                        <li><a href="<?php echo url('/product',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php elseif($son['template_type'] == 3): ?>
                        <li><a href="<?php echo url('/message',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php endif; endforeach; endif; else: echo "" ;endif; ?>
                    </ul>
                    <?php endif; ?>
                </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>
<div class="banner">
    <div id="carouselcaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselcaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselcaptions" data-slide-to="1" class=""></li>
            <li data-target="#carouselcaptions" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php if(is_array($bannerResult) || $bannerResult instanceof \think\Collection || $bannerResult instanceof \think\Paginator): $i = 0; $__LIST__ = $bannerResult;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            <div class="item">
                <a href="<?php echo $v['url']; ?>">
                <img class="pcImg" src="<?php echo $v['pc_url']; ?>">
                <img class="mobImg" src="<?php echo $v['m_url']; ?>">
                <div class="carousel-caption">
                    <h3><?php echo $v['title']; ?></h3>
                </div>
                </a>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
        <a class="left carousel-control" href="#carouselcaptions" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carouselcaptions" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<script>
    $(function () {
        $(".carousel-inner .item").first().addClass("active");
    })
</script>
<div class="applicationrange">
    <div class="container">
        <h2 class="text-center">应用范围</h2>
        <div class="line"></div>
        <div class="row">
            <div class="col-md-2 col-xs-4">
                <a href="#" class="thumbnail">
                    <img class="img-circle" src="/potatocms/public/static/index/images/icon/dianli.png" alt="应用范围">
                    <h3 class="text-center">应用范围</h3>
                </a>
            </div>
            <div class="col-md-2 col-xs-4">
                <a href="#" class="thumbnail">
                    <img class="img-circle" src="/potatocms/public/static/index/images/icon/jiancai.png" alt="应用范围">
                    <h3 class="text-center">应用范围</h3>
                </a>
            </div>
            <div class="col-md-2 col-xs-4">
                <a href="#" class="thumbnail">
                    <img class="img-circle" src="/potatocms/public/static/index/images/icon/jixie.png" alt="应用范围">
                    <h3 class="text-center">应用范围</h3>
                </a>
            </div>
            <div class="col-md-2 col-xs-4">
                <a href="#" class="thumbnail">
                    <img class="img-circle" src="/potatocms/public/static/index/images/icon/kuangshanjixie.png" alt="应用范围">
                    <h3 class="text-center">应用范围</h3>
                </a>
            </div>
            <div class="col-md-2 col-xs-4">
                <a href="#" class="thumbnail">
                    <img class="img-circle" src="/potatocms/public/static/index/images/icon/meitanxingye.png" alt="应用范围">
                    <h3 class="text-center">应用范围</h3>
                </a>
            </div>
            <div class="col-md-2 col-xs-4">
                <a href="#" class="thumbnail">
                    <img class="img-circle" src="/potatocms/public/static/index/images/icon/yejinkuangchan.png" alt="应用范围">
                    <h3 class="text-center">应用范围</h3>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="productcenter">
    <div class="container">
        <h2 class="text-center">产品中心</h2>
        <div class="line"></div>
        <div class="row productnav">
            <?php if(is_array($productSecondNav) || $productSecondNav instanceof \think\Collection || $productSecondNav instanceof \think\Paginator): $i = 0; $__LIST__ = $productSecondNav;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            <div class="col-md-2 col-xs-4">
                <a href="<?php echo url('/product',array('id'=>$v['id'])); ?>" class="btn btn-default btn-block">
                    <?php echo $v['column_name']; ?>
                </a>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
        <div class="row">
            <?php if(is_array($productContent) || $productContent instanceof \think\Collection || $productContent instanceof \think\Paginator): $i = 0; $__LIST__ = $productContent;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            <div class="col-md-3 col-xs-6">
                <a href="<?php echo url('/productdetails',array('id'=>$v['id'])); ?>" class="thumbnail">
                    <img  src="<?php echo $v['img_url']; ?>" alt="应用范围">
                    <h3 class="text-center"><?php echo $v['title']; ?></h3>
                </a>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
    </div>
</div>
<div class="productcenter">
    <div class="container">
        <h2 class="text-center">新闻资讯</h2>
        <div class="line"></div>
        <div class="row">
            <?php if(is_array($articleHeadlines) || $articleHeadlines instanceof \think\Collection || $articleHeadlines instanceof \think\Paginator): $i = 0; $__LIST__ = $articleHeadlines;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            <div class="col-md-3 col-xs-6">
                <a href="<?php echo url('/newsdetails',array('id'=>$v['id'])); ?>" class="thumbnail">
                    <img  src="<?php echo $v['img_url']; ?>" alt="应用范围">
                    <h3 class="text-center"><?php echo $v['title']; ?></h3>
                    <p><?php echo $v['seo_description']; ?></p>
                </a>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <div class="col-md-6 col-xs-12">
                <?php if(is_array($articleRecommend) || $articleRecommend instanceof \think\Collection || $articleRecommend instanceof \think\Paginator): $i = 0; $__LIST__ = $articleRecommend;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <div class="media">
                    <a href="<?php echo url('/newsdetails',array('id'=>$v['id'])); ?>">
                        <div class="media-left">
                            <img width="100" height="100" class="media-object" src="<?php echo $v['img_url']; ?>">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $v['title']; ?></h4>
                            <p><?php echo $v['seo_description']; ?></p>
                        </div>
                    </a>
                </div>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <?php if(is_array($columnName) || $columnName instanceof \think\Collection || $columnName instanceof \think\Paginator): $i = 0; $__LIST__ = $columnName;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            <div class="col-md-1 col-xs-2">
                <dl>
                    <dt><a href="<?php if($v['template_type'] == 0): ?><?php echo url('/page',array('id'=>$v['id'])); elseif($v['template_type'] == 1): ?><?php echo url('/article',array('id'=>$v['id'])); elseif($v['template_type'] == 2): ?><?php echo url('/product',array('id'=>$v['id'])); else: ?><?php echo url('/message',array('id'=>$v['id'])); endif; ?>"><?php echo $v['column_name']; ?></a></dt>
                    <?php if($v['children'] != 0): if(is_array($v['children']) || $v['children'] instanceof \think\Collection || $v['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $v['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$son): $mod = ($i % 2 );++$i;if($son['template_type'] == 0): ?>
                    <dd><a href="<?php echo url('/page',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></dd>
                    <dd role="separator" class="divider"></dd>
                    <?php elseif($son['template_type'] == 1): ?>
                    <dd><a href="<?php echo url('/article',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></dd>
                    <dd role="separator" class="divider"></dd>
                    <?php elseif($son['template_type'] == 2): ?>
                    <dd><a href="<?php echo url('/product',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></dd>
                    <dd role="separator" class="divider"></dd>
                    <?php elseif($son['template_type'] == 3): ?>
                    <dd><a href="<?php echo url('/message',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></dd>
                    <dd role="separator" class="divider"></dd>
                    <?php endif; endforeach; endif; else: echo "" ;endif; endif; ?>
                </dl>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <div class="col-md-2 col-xs-6">
                <a href="#" class="thumbnail">
                    <img  src="http://placehold.it/500x500/" alt="应用范围">
                    <h3 class="text-center">手机网站</h3>
                </a>
            </div>
            <div class="col-md-2 col-xs-6">
                <a href="#" class="thumbnail">
                    <img  src="http://placehold.it/500x500/" alt="应用范围">
                    <h3 class="text-center">微信</h3>
                </a>
            </div>
        </div>
        <p class="text-muted">咨询电话：<?php echo $setting['telephone']; ?><br/><?php echo $setting['record']; ?><br/><?php echo $setting['copyright']; ?></p>
    </div>
</footer>
</body>
<script type="text/javascript" src="/potatocms/public/static/index/js/common.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</html>