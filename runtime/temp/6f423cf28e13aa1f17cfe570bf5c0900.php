<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:75:"D:\phpS\PHPTutorial\WWW\potatocms/application/index\view\product\index.html";i:1588906506;s:73:"D:\phpS\PHPTutorial\WWW\potatocms\application\index\view\public\head.html";i:1588231261;s:73:"D:\phpS\PHPTutorial\WWW\potatocms\application\index\view\public\foot.html";i:1588045237;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $columnMessage['seo_title']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="keywords" content="<?php echo $columnMessage['seo_keywords']; ?>" />
    <meta name="description" content="<?php echo $columnMessage['seo_description']; ?>" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" >
    <link rel="stylesheet" href="/potatocms/public/static/index/common/website.css" >
    <script src="https://cdn.bootcss.com/jquery/2.2.2/jquery.js"></script>
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bsnavbar" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo url('/index'); ?>"><img src="<?php echo $setting['logo_url']; ?>" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bsnavbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<?php echo url('/index'); ?>">网站首页</a></li>
                <?php if(is_array($columnName) || $columnName instanceof \think\Collection || $columnName instanceof \think\Paginator): $i = 0; $__LIST__ = $columnName;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                <li class="dropdown">
                    <a href=" <?php if($v['template_type'] == 0): ?><?php echo url('/page',array('id'=>$v['id'])); elseif($v['template_type'] == 1): ?><?php echo url('/article',array('id'=>$v['id'])); elseif($v['template_type'] == 2): ?><?php echo url('/product',array('id'=>$v['id'])); else: ?><?php echo url('/message',array('id'=>$v['id'])); endif; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $v['column_name']; ?><span class="caret"></span></a>
                    <?php if($v['children'] != 0): ?>
                    <ul class="dropdown-menu" >
                        <?php if(is_array($v['children']) || $v['children'] instanceof \think\Collection || $v['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $v['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$son): $mod = ($i % 2 );++$i;if($son['template_type'] == 0): ?>
                        <li><a href="<?php echo url('/page',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php elseif($son['template_type'] == 1): ?>
                        <li><a href="<?php echo url('/article',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php elseif($son['template_type'] == 2): ?>
                        <li><a href="<?php echo url('/product',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php elseif($son['template_type'] == 3): ?>
                        <li><a href="<?php echo url('/message',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></li>
                        <li role="separator" class="divider"></li>
                        <?php endif; endforeach; endif; else: echo "" ;endif; ?>
                    </ul>
                    <?php endif; ?>
                </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>
<div class="banner">
    <div class="carousel slide">
        <div class="carousel-inner">
            <div class="item active">
                <img class="pcImg" src="<?php echo $columnMessage['img_url']; ?>">
                <img class="mobImg" src="<?php echo $columnMessage['m_url']; ?>">
                <div class="carousel-caption">
                    <h3><?php echo $columnMessage['seo_title']; ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo url('/index'); ?>">首页</a></li>
            <?php if(is_array($bread) || $bread instanceof \think\Collection || $bread instanceof \think\Paginator): $i = 0; $__LIST__ = $bread;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            <li><a href="<?php echo url('/product',array('id'=>$v['id'])); ?>"><?php echo $v['column_name']; ?></a></li>
            <?php endforeach; endif; else: echo "" ;endif; ?>
        </ol>
    </div>
</div>
<div class="productcenter">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="page-header">
                    <h1><?php echo $columnMessage['column_name']; ?><small>某某公司</small></h1>
                </div>
                <div class="row">
                    <?php if(is_array($product) || $product instanceof \think\Collection || $product instanceof \think\Paginator): $i = 0; $__LIST__ = $product;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <div class="col-md-3 col-xs-6">
                        <a href="<?php echo url('/productdetail',array('id'=>$v['id'])); ?>" class="thumbnail">
                            <img  src="<?php echo $v['img_url']; ?>" alt="<?php echo $v['title']; ?>">
                            <h3 class="text-center"><?php echo $v['title']; ?></h3>
                        </a>
                    </div>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </div>
                <?php echo $product->render(); ?>
            </div>
            <div class="col-md-2">
                <div class="list-group">
                    <a href="<?php echo url('/product',array('id'=>$rightNavFather['id'])); ?>" class="list-group-item active">
                        <?php echo $rightNavFather['column_name']; ?>
                    </a>
                    <?php if(is_array($rightNav) || $rightNav instanceof \think\Collection || $rightNav instanceof \think\Paginator): $i = 0; $__LIST__ = $rightNav;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;if($v['template_type'] == 0): ?>
                    <a href="<?php echo url('/page',array('id'=>$v['id'])); ?>" class="list-group-item"><?php echo $v['column_name']; ?></a>
                    <?php elseif($v['template_type'] == 1): ?>
                    <a href="<?php echo url('/article',array('id'=>$v['id'])); ?>" class="list-group-item"><?php echo $v['column_name']; ?></a>
                    <?php elseif($v['template_type'] == 2): ?>
                    <a href="<?php echo url('/product',array('id'=>$v['id'])); ?>" class="list-group-item"><?php echo $v['column_name']; ?></a>
                    <?php elseif($v['template_type'] == 3): ?>
                    <a href="<?php echo url('/message',array('id'=>$v['id'])); ?>" class="list-group-item"><?php echo $v['column_name']; ?></a>
                    <?php endif; endforeach; endif; else: echo "" ;endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <?php if(is_array($columnName) || $columnName instanceof \think\Collection || $columnName instanceof \think\Paginator): $i = 0; $__LIST__ = $columnName;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            <div class="col-md-1 col-xs-2">
                <dl>
                    <dt><a href="<?php if($v['template_type'] == 0): ?><?php echo url('/page',array('id'=>$v['id'])); elseif($v['template_type'] == 1): ?><?php echo url('/article',array('id'=>$v['id'])); elseif($v['template_type'] == 2): ?><?php echo url('/product',array('id'=>$v['id'])); else: ?><?php echo url('/message',array('id'=>$v['id'])); endif; ?>"><?php echo $v['column_name']; ?></a></dt>
                    <?php if($v['children'] != 0): if(is_array($v['children']) || $v['children'] instanceof \think\Collection || $v['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $v['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$son): $mod = ($i % 2 );++$i;if($son['template_type'] == 0): ?>
                    <dd><a href="<?php echo url('/page',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></dd>
                    <dd role="separator" class="divider"></dd>
                    <?php elseif($son['template_type'] == 1): ?>
                    <dd><a href="<?php echo url('/article',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></dd>
                    <dd role="separator" class="divider"></dd>
                    <?php elseif($son['template_type'] == 2): ?>
                    <dd><a href="<?php echo url('/product',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></dd>
                    <dd role="separator" class="divider"></dd>
                    <?php elseif($son['template_type'] == 3): ?>
                    <dd><a href="<?php echo url('/message',array('id'=>$son['id'])); ?>"><?php echo $son['column_name']; ?></a></dd>
                    <dd role="separator" class="divider"></dd>
                    <?php endif; endforeach; endif; else: echo "" ;endif; endif; ?>
                </dl>
            </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <div class="col-md-2 col-xs-6">
                <a href="#" class="thumbnail">
                    <img  src="http://placehold.it/500x500/" alt="应用范围">
                    <h3 class="text-center">手机网站</h3>
                </a>
            </div>
            <div class="col-md-2 col-xs-6">
                <a href="#" class="thumbnail">
                    <img  src="http://placehold.it/500x500/" alt="应用范围">
                    <h3 class="text-center">微信</h3>
                </a>
            </div>
        </div>
        <p class="text-muted">咨询电话：<?php echo $setting['telephone']; ?><br/><?php echo $setting['record']; ?><br/><?php echo $setting['copyright']; ?></p>
    </div>
</footer>
</body>
<script type="text/javascript" src="/potatocms/public/static/index/js/common.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</html>