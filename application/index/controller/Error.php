<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/5/12
 * Time: 9:47
 */

namespace app\index\controller;


use think\Controller;

class Error extends Controller
{
    public function _empty()
    {
        $this->redirect('Index/index');
    }
}