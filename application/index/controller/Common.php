<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/28
 * Time: 8:51
 */

namespace app\index\controller;


use think\Controller;

class Common extends Controller
{
    public function _empty(){
        $this->redirect("index");
    }
    public function _initialize()
    {
        $result=db("column")->where(array("is_hidden"=>0,"parent_id"=>0))->order("sort_order")->select()->toArray();
        foreach ($result as $k => $v) {
            $children=db('column')->where(array("is_hidden"=>0,'parent_id'=>$v['id']))->order("sort_order")->select()->toArray();
            if($children){
                $result[$k]['children']=$children;
            }else{
                $result[$k]['children']=0;
            }
        }
        $this->assign('columnName',$result);
        $settingResult=db("setting")->find();
        $this->assign('setting',$settingResult);
    }
}