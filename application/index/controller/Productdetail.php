<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/5/8
 * Time: 9:22
 */

namespace app\index\controller;


class Productdetail extends Common
{
    public function index(){
        $request=request();
        $data=$request->param();
        db("product_content")->where($data)->setInc('browse_volume');
        $column_id=db("product_content")->where($data)->value("column_id");
        $result=db("column")->where('id',$column_id)->find();
        $this->assign("columnMessage",$result);
        //获取面包屑
        $parent_id=$result['parent_id'];
        $arr=array();
        if($parent_id!=0){
            $resultBread= $this->recursiveBread($arr,$parent_id);
            array_push($resultBread,$result);
        }else{
            array_push($arr,$result);
            $resultBread=$arr;
        }
        $this->assign("bread",$resultBread);
        $result=db("product_content")->where($data)->find();
        $this->assign("product",$result);
        return $this->fetch();

    }
    public function recursiveBread($arr,$parent_id){
        $result= db("column")->where('id',$parent_id)->find();
        array_push($arr,$result);
        if($parent_id!=0){
            $this-> recursiveBread($arr,$result['parent_id']);
        }
        return $arr;
    }
}