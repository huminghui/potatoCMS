<?php
namespace app\index\controller;
class Index extends Common
{
    public function index()
    {
       $bannerResult= db("banner")->where(array("is_hidden"=>0))->select()->toArray();
       $this->assign("bannerResult",$bannerResult);
       $productSecondNav=db("column")->where(array("parent_id"=>86))->select()->toArray();
        $this->assign("productSecondNav",$productSecondNav);
        $productContent=db("product_content")->where('property','like','%c%')->limit(12)->select()->toArray();
        $this->assign("productContent",$productContent);
        $articleHeadlines=db("article_content")->where('property','like','%h%')->limit(2)->select()->toArray();
        $this->assign("articleHeadlines",$articleHeadlines);
        $articleRecommend=db("article_content")->where('property','like','%c%')->limit(3)->select()->toArray();
        $this->assign("articleRecommend",$articleRecommend);
        return $this->fetch();
    }
}
