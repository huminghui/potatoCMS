<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/5/7
 * Time: 14:57
 */

namespace app\index\controller;


class Product extends Common
{
    public function index(){
        $request=request();
        $data=$request->param();
        $result=db("column")->where('id',$data['id'])->find();
        $this->assign("columnMessage",$result);
        //获取面包屑
        $parent_id=$result['parent_id'];
        $arr=array();
        if($parent_id!=0){
            $resultBread= $this->recursiveBread($arr,$parent_id);
            array_push($resultBread,$result);
        }else{
            array_push($arr,$result);
            $resultBread=$arr;
        }
        $this->assign("bread",$resultBread);
        //获取右侧导航
        if($parent_id!=0){
            $sonColumn=db("column")->where("parent_id",$parent_id)->select()->toArray();
            $fatherColumn=db("column")->where("id",$parent_id)->find();
        }else{
            $sonColumn=db("column")->where("parent_id",$result['id'])->select()->toArray();
            $fatherColumn=$result;
        }
        $this->assign("rightNavFather",$fatherColumn);
        $this->assign("rightNav",$sonColumn);

        $parent_id=db("column")->where('id',$data['id'])->value("parent_id");
        if($parent_id==0){
            $allColumn= db("column")->where("parent_id",$data['id'])->column("id");
            $result=db("product_content")->where("column_id","IN",implode(',',$allColumn))->order("id desc")->paginate(12);
        }else{
            $result=db("product_content")->where("column_id",$data['id'])->order("id desc")->paginate(12);
        }
        $this->assign("product",$result);
        return $this->fetch();

    }
    public function recursiveBread($arr,$parent_id){
        $result= db("column")->where('id',$parent_id)->find();
        array_push($arr,$result);
        if($parent_id!=0){
            $this-> recursiveBread($arr,$result['parent_id']);
        }
        return $arr;
    }

}