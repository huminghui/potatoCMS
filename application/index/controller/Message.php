<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/5/8
 * Time: 10:35
 */

namespace app\index\controller;


class Message extends Common
{
    public function index(){
        $request=request();
        $data=$request->param();
        if($request->isGet()) {
            $result = db("column")->where('id', $data['id'])->find();
            $this->assign("columnMessage", $result);
            $parent_id = $result['parent_id'];
            //获取右侧导航
            if ($parent_id != 0) {
                $sonColumn = db("column")->where("parent_id", $parent_id)->select()->toArray();
                $fatherColumn = db("column")->where("id", $parent_id)->find();
            } else {
                $sonColumn = db("column")->where("parent_id", $result['id'])->select()->toArray();
                $fatherColumn = $result;
            }
            $this->assign("rightNavFather", $fatherColumn);
            $this->assign("rightNav", $sonColumn);
        }
        if($request->isPost()){
            $data['create_time']=date('Y-m-d H:i:s',time());
          $result= db("message_content")->insert($data);
            if($result){
                $this->success('留言成功');
            } else {
                $this->error('留言失败');
            }
        }
        return $this->fetch();
    }
}