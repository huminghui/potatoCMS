<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    'page'=>"index/page/index",
    'product'=>"index/product/index",
    'article'=>"index/article/index",
    'productdetail'=>"index/productdetail/index",
    'newsdetail'=>"index/newsdetail/index",
    'message'=>"index/message/index"
];
