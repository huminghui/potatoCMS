<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/18
 * Time: 9:59
 */

namespace app\admin\controller;

use app\admin\model\AuthRule as AuthRuleModel;
use app\admin\validate\AuthRule as AuthRuleValidate;
class AuthRule extends Common
{
    public function lst(){
        return $this->fetch();
    }
    public function listAll(){
        $authRuleModel= new AuthRuleModel();
        $result= $authRuleModel->selectData();
        return json($result);
    }
    public function add(){
        $request=request();
        $data= $request->param();
        $authRuleModel=new AuthRuleModel();
        if($request->isGet()){
            $authRule =$authRuleModel->selectData();
            $this->assign("authRule",$authRule);
        }
        if($request->isPost()){
            $authRuleValidate=new AuthRuleValidate();
            if(!$authRuleValidate->check($data)){
                $this->error($authRuleValidate->getError());
            }
            $result= $authRuleModel->saveData($data);
            if($result){
                $this->success('权限添加成功', 'lst');
            } else {
                $this->error('权限添加失败');
            }
        }
        return $this->fetch();
    }
    public function edit(){
        $request=request();
        $data=$request->param();
        $authRuleModel=new AuthRuleModel();
        if($request->isPost()){
            $authRuleValidate=new AuthRuleValidate();
            if(!$authRuleValidate->check($data)){
                $this->error($authRuleValidate->getError());
            }
            $result= $authRuleModel->updateData($data);
            if($result){
                $this->success('权限更新成功', 'lst');
            } else {
                $this->error('权限更新失败');
            }
        }

        if($request->isGet()){
            $authRuleAll =$authRuleModel->selectData();
            $this->assign("authRuleAll",$authRuleAll);
            $authRuleModel= $authRuleModel->readDataById($data);
            $this->assign("authRule",$authRuleModel);
        }
        return $this->fetch();
    }
    public function del(){
        $request=request();
        $data=$request->param("id");
        $authRuleModel= new AuthRuleModel();
        $result= $authRuleModel->delDataById($data);
        if($result){
            $this->success('权限删除成功', 'lst');
        } else {
            $this->error('权限删除失败');
        }
    }
    /*
 * 根据传过来的id删除所有数据
 */
    public function delall(){
        $request=request();
        $data =$request->param();
        $authRuleModel= new AuthRuleModel();
        $result= $authRuleModel->delAllData($data['allId']);
        if($result){
            return json(["code"=>0,"msg"=>"删除成功"]);
        } else {
            return json(["code"=>-1,"msg"=>"删除失败"]);
        }
    }
}