<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/1
 * Time: 14:50
 */

namespace app\admin\controller;
use app\admin\model\Column as ColumnModel;
use app\admin\model\ProductContent as ProductContentModel;
use app\admin\model\ArticleContent as ArticleContentModel;
use app\admin\model\MessageContent as MessageContentModel;
use app\admin\model\SingleContent as SingleContentModel;
use app\admin\validate\Content as ContentValidate;
use think\Controller;

class Content extends Common
{
    /*
     * 内容管理树形分类加列表页
     */
    public function lst(){
        return $this->fetch();
    }
    /*
    * 获取ztree栏目列表
    */
    public function ztreeAjax(){
        $columnModel= new ColumnModel();
        $result=$columnModel->ztreeForm();
        return json($result);
    }
    /*
     * 内容管理列表页
     */
    public function productlist(){
        $request=request();
        $data=$request->param();
        if($request->isGet()){
            //获取该栏目名称
            $columnModel= new ColumnModel();
            if($data){
                $columnContent= $columnModel->getColumn($data);
            }else{
                $columnContent= $columnModel->getTopProColumn($data);
            }
            $this->assign('columnContent',$columnContent);
        }
        return $this->fetch();
    }
    /**
     * 获取产品列表的内容，返回json数据
     */
    public function listall(){
        $request=request();
        $data=$request->param("id");
        //获取栏目下的所有id
        $columnModel= new ColumnModel();
        $columnAllId= $columnModel->getColumnAllId($data);
        $productContentModel=new ProductContentModel();
        $result=$productContentModel->getAllLists($columnAllId);
        return json($result);
    }
    /**
     * 获取文章列表的内容，返回json数据
     */
    public function listallArticle(){
        $request=request();
        $data=$request->param("id");
        //获取栏目下的所有id
        $columnModel= new ColumnModel();
        $columnAllId= $columnModel->getColumnAllId($data);
        $articleContentModel=new ArticleContentModel();
        $result=$articleContentModel->getAllLists($columnAllId);
        return json($result);
    }
    /**
     * 获取文章列表的内容，返回json数据
     */
    public function listallMessage(){
        $request=request();
        $data=$request->param();
        $messageContentModel=new MessageContentModel();
        $result=$messageContentModel->getAllLists($data);
        return json($result);
    }
    /*
     * 获取栏目名称
     */
    public function getColumnName(){
        $data=request()->param();
        $columnModel=new ColumnModel();
        $result=$columnModel->getColumn($data);
        return json($result);
    }
    /*
     * 增加产品列表内容
     */
    public function addproduct(){
        $request=request();
        $data =$request->param();
        //把所有产品栏目类型，输出到前台。
        $columnModel= new ColumnModel();
        $proColumn=$columnModel->getProColumn();
        $this->assign('proColumn',$proColumn);
        /**
         * post提交栏目数据
         */
        if($request->isPost()){
            $data=$request->param();
            $data=$this->upload($data);
            $contentValidate=new ContentValidate();
            if(!$contentValidate->scene("productAdd")->check($data)){
                $this->error($contentValidate->getError());
            }
            $productContentModel=new ProductContentModel();
            $result= $productContentModel->saveData($data);
            if($result){
                $this->success('内容添加成功', url("productlist",array('id'=>$data["column_id"])));
            } else {
                $this->error('内容添加失败');
            }
        }
        return $this->fetch();
    }
    /*
     * 修改产品列表内容
     */
    public function editproduct(){
        $request=request();
        $data =$request->param();
        if($request->isGet()){
            //获取产品详情
            $productContentModel=new ProductContentModel();
            $productContent=$productContentModel->getProContent($data);
            $this->assign('productContent',$productContent);
            //把所有产品栏目类型，输出到前台。
            $columnModel= new ColumnModel();
            $proColumn=$columnModel->getProColumn();
            $this->assign('proColumn',$proColumn);
        }
        if($request->isPost()){
            $data=$this->upload($data);
            $contentValidate=new ContentValidate();
            if(!$contentValidate->scene("productEdit")->check($data)){
                $this->error($contentValidate->getError());
            }
            $productContentModel=new ProductContentModel();
            $result= $productContentModel->updateData($data);
            if($result){
                $this->success('内容修改成功', url("productlist",array('id'=>$data["column_id"])));
            } else {
                $this->error('内容修改失败');
            }

        }
        return $this->fetch();
    }
    /*
     * 删除单个产品
     */
    public function deleteProduct(){
        $request=request();
        $data =$request->param();
        $productContentModel=new ProductContentModel();
        $result= $productContentModel->delData($data);
        if($result){
            $this->success('内容删除成功');
        } else {
            $this->error('内容删除失败');
        }
    }
    /*
     * 根据传过来的id删除所有数据
     */
    public function delallproduct(){
        $request=request();
        $data =$request->param();
        $productContentModel=new ProductContentModel();
        $result= $productContentModel->delAllData($data['allId']);
        if($result){
            return json(["code"=>0,"msg"=>"删除成功"]);
        } else {
            return json(["code"=>-1,"msg"=>"删除失败"]);
        }
    }
    /*
    * 文章管理列表页
    */
    public function articlelist(){
        $request=request();
        $data=$request->param();
        if($request->isGet()){
            //获取该栏目名称
            $columnModel= new ColumnModel();
            if($data){
                $columnContent= $columnModel->getColumn($data);
            }else{
                $columnContent= $columnModel->getTopArtiColumn($data);
            }
            $this->assign('columnContent',$columnContent);
        }
        return $this->fetch();
    }
    /*
 * 增加文章列表内容
 */
    public function addarticle(){
        $request=request();
        $data =$request->param();
        //把所有产品栏目类型，输出到前台。
        $columnModel= new ColumnModel();
        $artiColumn=$columnModel->getArtiColumn();
        $this->assign('artiColumn',$artiColumn);
        /**
         * post提交栏目数据
         */
        if($request->isPost()){
            $data=$request->param();
            $data=$this->upload($data);
            $contentValidate=new ContentValidate();
            if(!$contentValidate->scene("productAdd")->check($data)){
                $this->error($contentValidate->getError());
            }
            $articleContentModel=new ArticleContentModel();
            $result= $articleContentModel->saveData($data);
            if($result){
                $this->success('内容添加成功', url("articlelist",array('id'=>$data["column_id"])));
            } else {
                $this->error('内容添加失败');
            }
        }
        return $this->fetch();
    }
    /*
     * 修改产品文章列表内容
     */
    public function editarticle(){
        $request=request();
        $data =$request->param();
        if($request->isGet()){
            //获取产品详情
            $articleContentModel=new ArticleContentModel();
            $articleContent=$articleContentModel->getArtiContent($data);
            $this->assign('articleContent',$articleContent);
            //把所有产品栏目类型，输出到前台。
            $columnModel= new ColumnModel();
            $artiColumn=$columnModel->getArtiColumn();
            $this->assign('artiColumn',$artiColumn);
        }
        if($request->isPost()){
            $data=$this->upload($data);
            $contentValidate=new ContentValidate();
            if(!$contentValidate->scene("articleEdit")->check($data)){
                $this->error($contentValidate->getError());
            }
            $articleContentModel=new ArticleContentModel();
            $result= $articleContentModel->updateData($data);
            if($result){
                $this->success('内容修改成功', url("articlelist",array('id'=>$data["column_id"])));
            } else {
                $this->error('内容修改失败');
            }

        }
        return $this->fetch();
    }
    /*
     * 删除单个文章
     */
    public function deleteArticle(){
        $request=request();
        $data =$request->param();
        $articleContentModel=new ArticleContentModel();
        $result= $articleContentModel->delData($data);
        if($result){
            $this->success('内容删除成功');
        } else {
            $this->error('内容删除失败');
        }
    }
    /*
     * 根据传过来的id删除所有文章数据
     */
    public function delallArticle(){
        $request=request();
        $data =$request->param();
        $articleContentModel=new ArticleContentModel();
        $result= $articleContentModel->delAllData($data['allId']);
        if($result){
            return json(["code"=>0,"msg"=>"删除成功"]);
        } else {
            return json(["code"=>-1,"msg"=>"删除失败"]);
        }
    }
    /*
     * 增加单页内容
     */
    public function singlepage(){
        $request=request();
        $data=$request->param();
        $singleContentModel = new SingleContentModel();
        if($request->isGet()){
            //获取该栏目名称
            $columnModel= new ColumnModel();
            $columnContent= $columnModel->getColumn($data);
            $this->assign('columnContent',$columnContent);
            $arrData=array();
            $arrData["column_id"]=$data["id"];
            $singleContent=$singleContentModel->getContent($arrData);
            $this->assign('singleContent',$singleContent);
        }
        if($request->post()){
            $contentValidate=new ContentValidate();
            if(!$contentValidate->scene("single")->check($data)){
                $this->error($contentValidate->getError());
            }
            if($data['id']){
                $result=$singleContentModel->updateData($data);
                if($result){
                    $this->success('内容修改成功');
                } else {
                    $this->error('内容修改失败');
                }
            }else{
                $result=$singleContentModel->saveData($data);
                if($result){
                    $this->success('内容添加成功');
                } else {
                    $this->error('内容添加失败');
                }
            }


        }
        return $this->fetch();
    }
    /*
    * 留言列表页
    */
    public function messagelist(){
        $request=request();
        $data=$request->param();
        if($request->isGet()){
            //获取该栏目名称
            $columnModel= new ColumnModel();
            if($data){
                $columnContent= $columnModel->getColumn($data);
            }else{
                $columnContent= $columnModel->getTopMesColumn($data);
            }
            $this->assign('columnContent',$columnContent);
        }
        return $this->fetch();
    }
    /*
     * 删除单个留言
     */
    public function deleteMessage(){
        $request=request();
        $data =$request->param();
        $messageContentModel=new MessageContentModel();
        $result= $messageContentModel->delData($data);
        if($result){
            $this->success('留言删除成功');
        } else {
            $this->error('留言删除失败');
        }
    }
    /*
    * 根据传过来的id删除所有数据
    */
    public function delallmessage(){
        $request=request();
        $data =$request->param();
        $messageContentModel=new MessageContentModel();
        $result= $messageContentModel->delAllData($data['allId']);
        if($result){
            return json(["code"=>0,"msg"=>"删除成功"]);
        } else {
            return json(["code"=>-1,"msg"=>"删除失败"]);
        }
    }
}