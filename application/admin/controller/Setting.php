<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/17
 * Time: 14:02
 */

namespace app\admin\controller;

use app\admin\model\Setting as SettingModel;
class Setting extends Common
{
    public function index(){
        $settingModel= new SettingModel();
        $settingMessage= $settingModel->readAllData();
        if($settingMessage){
         $this->assign("settingMessage",$settingMessage);
        }
        return $this->fetch();
    }
    public function add(){
        $request=request();
        $data = $request->param();
        $settingModel= new SettingModel();
        if($request->isPost()){
            $data=$this->upload($data);
            if($data['id']){
                $result= $settingModel->updateData($data);
                if($result){
                    $this->success('内容更新成功', 'index');
                } else {
                    $this->error('内容更新失败');
                }
            }else{
                $result= $settingModel->saveData($data);
                if($result){
                    $this->success('内容添加成功', 'index');
                } else {
                    $this->error('内容添加失败');
                }
            }
        }
    }
}