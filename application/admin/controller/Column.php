<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2019/10/30
 * Time: 16:22
 */

namespace app\admin\controller;
use think\Controller;
use think\Request;
use app\admin\model\Column as ColumnModel;
use app\admin\validate\Column as ColumnValidate;
class Column extends Common
{
    /*
     * 列表页
     */
    public function lst(){
        $columnModel= new ColumnModel();
        $result=$columnModel->readAll();
        $this->assign('columnResult',$result);
        return $this->fetch();
    }

    /*
     * 添加页面
     */
    public function add(){
        $request = request();
        $columnModel= new ColumnModel();
        /**
         * get增加栏目或增加子栏目
         */
        if($request->isGet()){
            $data=$request->param();
            /*
             * id=0表示添加顶级栏目，不等于0添加子栏目
             */
            if($data['id']!="0"){
                $result=$columnModel->getColumn($data);
                $this->assign('columnResult',$result);
            }else{
                $result=array("id"=>0,"column_name"=>"顶级栏目");
                $this->assign('columnResult',$result);
            }
        }
        /**
         * post提交栏目数据
         */
        if($request->isPost()){
            $data=$request->param();
            $data=$this->upload($data);
            $columnValidate=new ColumnValidate();
            if(!$columnValidate->check($data)){
                $this->error($columnValidate->getError());
            }
            $result= $columnModel->saveData($data);
            if($result){
                $this->success('栏目添加成功', 'lst');
            } else {
                $this->error('栏目添加失败');
            }
        }

        return $this->fetch();
    }
    /*
     * 修改页面
     */
    public function edit(){
        $request = request();
        $columnModel= new ColumnModel();
        if($request->isGet()){
            $data=$request->param();
            $resultAllColumn=$columnModel->readAll();
            $result=$columnModel->getColumn($data);
            $this->assign('resultAllColumn',$resultAllColumn);
            $this->assign('columnResult',$result);
        }
        if($request->isPost()){
            $data=$request->param();
            $data=$this->upload($data);
            $columnValidate=new ColumnValidate();
            if(!$columnValidate->check($data)){
                $this->error($columnValidate->getError());
            }
            $result=$columnModel->updateColumn($data);
            if($result){
                $this->success('栏目更新成功', 'lst');
            } else {
                $this->error('栏目更新失败');
            }
        }
        return $this->fetch();
    }
    /*
     * 删除页面
     */
    public function del(){
        $request=request();
        $data = $request->param("id");
        $columnModel= new ColumnModel();
        $result=$columnModel->delColumn($data);
        if($result){
            $this->success('栏目删除成功', 'lst');
        } else {
            $this->error('栏目删除失败');
        }

    }
    /*
     * 修改排序
     */
    public function updateColumnSort(){
        $data =request()->param();
        $columnModel= new ColumnModel();
        $result=$columnModel->updateColumn($data);
        if($result){
            return json(['code'=>0,'msg'=>"栏目排序修改成功"]);
        } else {
            return json(['code'=>1,'msg'=>"栏目排序修改失败"]);
        }
    }
}