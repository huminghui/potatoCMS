<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/17
 * Time: 16:54
 */

namespace app\admin\controller;

use app\admin\model\User as UserModel;
use think\captcha\Captcha;
use think\Controller;

class Login extends Controller
{
    public function index(){
        $captcha = new Captcha();
        $request=request();
        if($request->isPost()){
            if (!$captcha->check(input("captcha"))){
                $this->error("验证码错误");
            }
            $user = new UserModel();
            $data=$request->param();
            $result= $user->login($data);
            if($result==1){
                $this->error('用户名不存在');
            }elseif($result==2){
                $this->error('密码错误');
            }if($result==3){
                $this->success('登录成功，正在为您跳转','index/index');
            }
        }
        return view();
    }
}