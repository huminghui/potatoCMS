<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/18
 * Time: 10:09
 */

namespace app\admin\controller;

use app\admin\model\AuthGroup as AuthGroupModel;
use app\admin\model\AuthRule as AuthRuleModel;
use app\admin\validate\AuthGroup as AuthGroupValidate;
class AuthGroup extends Common
{
    public function lst(){
        return $this->fetch();
    }
    public function add(){
        $request=request();
        $data=$request->param();
        if($request->isPost()){
           $authGroupModel= new AuthGroupModel();
           $authGroupValidate= new AuthGroupValidate();
            if(!$authGroupValidate->check($data)){
                $this->error($authGroupValidate->getError());
            }
           $result=$authGroupModel->saveData($data);
            if($result){
                $this->success('用户组添加成功', 'lst');
            } else {
                $this->error('用户组添加失败');
            }
        }
        return $this->fetch();
    }
    public function edit(){
        $request=request();
        $data=$request->param();
        if($request->isGet()){
            $authGroupModel= new AuthGroupModel();
            $authGroup=$authGroupModel->readDataById($data);
            $this->assign("authGroup",$authGroup);
        }
        if($request->isPost()){
            $authGroupModel= new AuthGroupModel();
            $authGroupValidate= new AuthGroupValidate();
            if(!$authGroupValidate->check($data)){
                $this->error($authGroupValidate->getError());
            }
            $result=$authGroupModel->updateData($data);
            if($result){
                $this->success('用户组修改成功', 'lst');
            } else {
                $this->error('用户组修改失败');
            }
        }
        return $this->fetch();
    }
    public function del(){
        $request=request();
        $data=$request->param();
        $authGroupModel= new AuthGroupModel();
        $result= $authGroupModel->delDataById($data);
        if($result){
            $this->success('用户组删除成功', 'lst');
        } else {
            $this->error('用户组删除失败');
        }
    }
    /*
* 根据传过来的id删除所有数据
*/
    public function delall(){
        $request=request();
        $data =$request->param();
        $authGroupModel= new AuthGroupModel();
        $result= $authGroupModel->delAllData($data['allId']);
        if($result){
            return json(["code"=>0,"msg"=>"删除成功"]);
        } else {
            return json(["code"=>-1,"msg"=>"删除失败"]);
        }
    }
    public function listAll(){
        $authGroupModel= new AuthGroupModel();
        $result= $authGroupModel->readAllData();
        return json($result);
    }
        /*
    * 获取ztree权限列表
    */
    public function ztreeAjax(){
        $authRuleModel= new AuthRuleModel();
        $result=$authRuleModel->ztreeForm();
        return json($result);
    }
}