<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/17
 * Time: 9:32
 */

namespace app\admin\controller;

use app\admin\model\Banner as BannerModel;
use app\admin\validate\Banner as BannerValidate;
class Banner extends Common
{
    public function lst(){
        return $this->fetch();
    }
    public function listAll(){
        $bannerModel= new BannerModel();
        $result= $bannerModel->readAllData();
        return json($result);
    }
    public function add(){
        $request=request();
        $data=$request->param();
        $bannerValidate=new BannerValidate();

        if($request->isPost()){
            $data=$this->upload($data);
            if(!$bannerValidate->scene("add")->check($data)){
                $this->error($bannerValidate->getError());
            }
            $bannerModel= new BannerModel();
            $result= $bannerModel->saveData($data);
            if($result){
                $this->success('内容添加成功', 'lst');
            } else {
                $this->error('内容添加失败');
            }
        }
        return $this->fetch();
    }
    public function edit(){
        $request=request();
        $data=$request->param();
        $bannerValidate=new BannerValidate();
        if($request->isPost()){
            $data=$this->upload($data);
            $bannerModel= new BannerModel();
            if(!$bannerValidate->scene("edit")->check($data)){
                $this->error($bannerValidate->getError());
            }
            $result= $bannerModel->updateData($data);
            if($result){
                $this->success('内容更新成功', 'lst');
            } else {
                $this->error('内容更新失败');
            }
        }

        if($request->isGet()){
            $bannerModel= new BannerModel();
            $bannerMessage= $bannerModel->readDataById($data);
            $this->assign("bannerMessage",$bannerMessage);
        }
        return $this->fetch();
    }
    public function del(){
        $request=request();
        $data=$request->param();
        $bannerModel= new BannerModel();
        $result= $bannerModel->delDataById($data);
        if($result){
            $this->success('内容删除成功', 'lst');
        } else {
            $this->error('内容删除失败');
        }
    }
    /*
 * 根据传过来的id删除所有数据
 */
    public function delall(){
        $request=request();
        $data =$request->param();
        $bannerModel= new BannerModel();
        $result= $bannerModel->delAllData($data['allId']);
        if($result){
            return json(["code"=>0,"msg"=>"删除成功"]);
        } else {
            return json(["code"=>-1,"msg"=>"删除失败"]);
        }
    }
}