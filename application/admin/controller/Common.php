<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/13
 * Time: 10:59
 */

namespace app\admin\controller;


use think\Controller;
use think\Request;

class Common extends Controller
{
    public function _initialize(){
        if (!session('uid') || !session('username')) {
            $this->error('您尚未登录系统', url('login/index'));
        }
        $auth=new Auth();
        $request=Request::instance();        $con=$request->controller();
        $action=$request->action();
        $name=$con.'/'.$action;

        $notCheck=array('Index/index','Index/welcome','Index/logout','Banner/listall','User/listall',
                        'AuthGroup/listall','AuthRule/listall','AuthGroup/ztreeajax','Content/ztreeajax',
                        'User/getgroupname','Content/listallarticle','Content/listallmessage','Content/listall',
                        'Content/delallarticle','Content/delallmessage','Content/delallproduct','AuthRule/delall',
                        'AuthGroup/delall','Banner/delall','Column/updatecolumnsort',"Content/getcolumnname");
        if(!in_array($name, $notCheck)){
            if(!$auth->check($name,session('uid'))){
              $this->error("您没有权限操作","index/welcome");
            }
        }

    }
    /*
    * 上传文件有个疑问？ppt和psd格式的文件$file 为NULL
    */
    public function upload($data){
        $files=request()->file();
        if($files){
            foreach ($files as $key=>$file){
                $info=$file->validate(['size'=>1567800,'ext'=>'jpg,png,gif'])->move(ROOT_PATH.'public'.DS.'uploads');
                if($info){
                    $data[$key]='/potatocms/public/uploads/'.$info->getSaveName();
                }else{
                    // 上传失败获取错误信息
                    $msg= $file->getError();
                    $this->error($msg);
                }
            }
            return $data;
        }else{
            return $data;
        }

    }


}