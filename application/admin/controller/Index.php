<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2019/10/30
 * Time: 16:22
 */
namespace app\admin\controller;
use think\Controller;
use think\Db;

class Index extends Common
{
    public function index()
    {
        return $this->fetch("index");
    }
    public function welcome()
    {
        $info = array(
            "article"=> Db::name("product_content")->count()+Db::name("article_content")->count(),
            "banner"=> Db::name("banner")->count(),
            "message"=> Db::name("message_content")->count(),
            "column"=> Db::name("column")->count(),
            'os'=>PHP_OS,
            'environment'=>$_SERVER["SERVER_SOFTWARE"],
            'operationmode'=>php_sapi_name(),
            'tpversion'=>THINK_VERSION.' [ <a href="http://thinkphp.cn" target="_blank">查看最新版本</a> ]',
            'uploadlimit'=>ini_get('upload_max_filesize'),
            'timelimit'=>ini_get('max_execution_time').'秒',
            'servertime'=>date("Y年n月j日 H:i:s"),
            'beijingtime'=>gmdate("Y年n月j日 H:i:s",time()+8*3600),
            'serveip'=>$_SERVER['SERVER_NAME'].' [ '.gethostbyname($_SERVER['SERVER_NAME']).' ]',
            'remainspace'=>round((disk_free_space(".")/(1024*1024)),2).'M',
            'register_globals'=>get_cfg_var("register_globals")=="1" ? "ON" : "OFF",
            'magic_quotes_gpc'=>(1===get_magic_quotes_gpc())?'YES':'NO',
            'magic_quotes_runtime'=>(1===get_magic_quotes_runtime())?'YES':'NO',
        );
        $this->assign('info',$info);
        return $this->fetch("welcome");
    }
    public function logout(){
        session(null);
        $this->success('退出系统成功！',url('login/index'));
    }
}
