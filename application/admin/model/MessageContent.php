<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/13
 * Time: 11:18
 */

namespace app\admin\model;


use think\Model;

class MessageContent extends Model
{
    /*
     * 保存数据
     */
    public function saveData($data){
        $data['create_time']=date('Y-m-d H:i:s',time());
        if(array_key_exists('property',$data)){
            $data['property']=implode(",",$data['property']);
        }
        $result=$this->save($data);
        return $result;
    }
    /*
     * 更新数据
     */
    public function updateData($data){
        $data['update_time']=date('Y-m-d H:i:s',time());
        $result = $this->update($data);
        return $result;
    }
    /*
     * 删除数据
     */
    public function delData($data){
        $result = $this->where($data)->delete();
        return $result;
    }
    public function delAllData($data){
        $result = $this->where('id','in',$data)->delete();
        return $result;
    }
    /*
     * 获取栏目所有数据
     */
    public function getAllLists($data){
        $result=$this->where("column_id",$data['id'])->order('id desc')->select()->toArray();
        return $result;
    }

}