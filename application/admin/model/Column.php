<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2019/10/30
 * Time: 19:13
 */

namespace app\admin\model;

use think\Model;
class Column extends Model
{
    /*
     * 保存数据
     */
    public  function saveData($data){
        $data['create_time']=date('Y-m-d H:i:s',time());
        $result = $this->save($data);
        return $result;
    }
    /*
     * 读取所有数据
     */
    public function readAll(){
        $result=$this->order("sort_order asc")->select()->toArray();
        return $this->sort($result);
    }
    /*
     * 更新数据
     */
    public function updateColumn($data){
        $data['update_time']=date('Y-m-d H:i:s',time());
        $result = $this->update($data);
        return $result;

    }
    /*
     * 获取一条数据
     */
    public function getColumn($data){
        $result = $this->get($data)->toArray();
        return $result;

    }
    /*
     * 所有数据排序
     */
    public function sort($data,$pid=0,$level=0){
        static $arr=array();
        foreach($data as $k=>$v){
            if($v['parent_id']==$pid){
                $v['level'] = $level;
                $arr[] = $v;
                $this->sort($data,$v['id'],$level+1);
            }
        }
        return $arr;
    }
    /*
     * 删除栏目
     */
    public function delColumn($data){
        $result = $this->select()->toArray();
        //获取所有子元素
        $childrenColumn=$this->_getChildren($result,$data);
        //把父元素的id加进去
        array_push($childrenColumn,$data);
        $result=Column::destroy($childrenColumn);
        return $result;

    }
    public function _getChildren($data,$columnId){
        static  $arr = [];
        foreach ($data as $key =>$val){
            //如果栏目的pid等于传过来的cateId 那就是所传的id的子栏目
            if($val['parent_id'] == $columnId){
                $arr[] = $val['id'];
                $this->_getChildren($data,$val['id']);
            }
        }
        return $arr;
    }
    /*
        * 获取ztree格式数据
        */
    public function ztreeForm(){
        $result=$this->field('id,parent_id,column_name,template_type')->select()->toArray();
        return $this->formatZtree($result);
    }
    /*
    * 格式化成ztree数据
    */
    public function formatZtree($data){
        static $arr=array();
        foreach($data as $k=>$v){
            if($v['parent_id']==0){
                $v['open']=true;
            }
            $v['name']=$v['column_name'];
            $v['pId']=$v['parent_id'];
            $v['target']="mainiframe";
            unset($v['parent_id']);
            unset($v['column_name']);
            if($v['template_type']==0){
                $v['url']="singlepage/id/".$v['id'];
            }elseif($v['template_type']==1){
                $v['url']="articlelist/id/".$v['id'];
            }elseif($v['template_type']==2){
                $v['url']="productlist/id/".$v['id'];
            }else{
                $v['url']="messagelist/id/".$v['id'];
            }
            $arr[]=$v;
        }
        return $arr;
    }
    /*
     * 获取产品栏目顶级栏目
     */
    public function getTopProColumn(){
        $result=$this->where('parent_id=0 AND template_type=2')->find();
        return $result;
    }
    /*
     * 获取产品栏目列表
     */
    public  function getProColumn(){
        $result=$this->where('template_type=2')->select()->toArray();
        return $this->sort($result);
    }
    /*
     * 获取产品栏目顶级栏目
     */
    public function getTopArtiColumn(){
        $result=$this->where('parent_id=0 AND template_type=1')->find();
        return $result;
    }
    /*
  * 获取产品栏目顶级栏目
  */
    public function getTopMesColumn(){
        $result=$this->where('parent_id=0 AND template_type=3')->find();
        return $result;
    }
    /*
     * 获取产品栏目列表
     */
    public  function getArtiColumn(){
        $result=$this->where('template_type=1')->select()->toArray();
        return $this->sort($result);
    }
    /*
     * 获取栏目id的所有子元素id
     */
    public function getColumnAllId($data){
        $result = $this->select()->toArray();
        //获取所有子元素
        $childrenColumn=$this->_getChildren($result,$data);
        //把父元素的id加进去
        array_push($childrenColumn,$data);
        return $childrenColumn;
    }

}