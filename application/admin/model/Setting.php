<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/17
 * Time: 14:03
 */

namespace app\admin\model;


use think\Model;

class Setting extends Model
{
    /*
     * 保存数据
     */
    public function saveData($data){
        $data['create_time']=date('Y-m-d H:i:s',time());
        return $this->save($data);
    }
    /*
     * 更新数据
     */
        public function updateData($data){
            $data['update_time']=date('Y-m-d H:i:s',time());
            $result = $this->update($data);
            return $result;

        }
    /*
* 读取所有数据
*/
    public function readAllData(){
        return $this->find()->toArray();
    }
}