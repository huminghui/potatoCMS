<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/17
 * Time: 9:34
 */

namespace app\admin\model;


use think\Model;

class Banner extends Model
{
    /*
   * 保存数据
   */
    public function saveData($data){
        $data['create_time']=date('Y-m-d H:i:s',time());
        return $this->save($data);
    }
    /*
   * 读取所有数据
   */
    public function readAllData(){
        return $this->select()->toArray();
    }
    /*
   * 根据id查询数据
   */
    public function readDataById($data){
        return $this->where($data)->find()->toArray();
    }
    /*
     * 更新数据
     */
    public function updateData($data){
        $data['update_time']=date('Y-m-d H:i:s',time());
        $result = $this->update($data);
        return $result;

    }
    /*
     * 根据id删除数据
     */
    public function delDataById($data){
        return $this->where($data)->delete();
    }
    /*
    * 根据id删除所有数据
    */
    public function delAllData($data){
        $result = $this->where('id','in',$data)->delete();
        return $result;
    }
}