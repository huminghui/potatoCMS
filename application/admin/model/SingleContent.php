<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/8
 * Time: 16:50
 */

namespace app\admin\model;


use think\Model;

class SingleContent extends Model
{
    //保存数据
    public function saveData($data){
        $data['create_time']=date('Y-m-d H:i:s',time());
        $result = $this->save($data);
        return $result;
    }
    /*
   * 更新数据
   */
    public function updateData($data){
        $data['update_time']=date('Y-m-d H:i:s',time());
        $result = $this->update($data);
        return $result;

    }
    //获取一条数据
    public function getContent($data)
    {
        $result = $this->get($data);
        return $result;
    }
}