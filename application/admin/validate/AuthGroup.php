<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/18
 * Time: 10:04
 */

namespace app\admin\validate;

use think\Validate;

class AuthGroup extends Validate
{
    protected $rule=[
        'title'=>'require',
        'rules'=>'require',
    ];
    protected $message=[
        'title.require' => '请填写用户组名称',
        'rules.require' => '请选择用户的权限',
    ];
}