<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/17
 * Time: 10:29
 */

namespace app\admin\validate;


use think\Validate;

class Banner extends Validate
{
    protected $rule=[
        'title'=>'require',
        'pc_url'=>'require',
        'm_url'=>'require',
    ];
    protected $message=[
        'title.require' => '请填写标题名称',
        'pc_url.require' => '请上传电脑端图片',
        'm_url.require' => '请上传手机端图片',
    ];
    protected $scene=[
        "add"=>['title','pc_url','m_url'],
        "edit"=>['title'],
    ];
}