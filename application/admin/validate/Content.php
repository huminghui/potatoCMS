<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/16
 * Time: 10:24
 */

namespace app\admin\validate;


use think\Validate;

class Content extends Validate
{
    protected $rule=[
        'title'=>'require',
        'author'=>'require',
        'img_url'=>'require',
        'content'=>'require',
    ];
    protected $message=[
        'title.require' => '请填写标题名称',
        'author.require' => '请填写作者',
        'img_url.require' => '请上传栏目图片',
        'content.require' => '请填内容详情',
    ];
    protected $scene=[
        "productAdd"=>['title','author','img_url','content'],
        "productEdit"=>['title','author','content'],
        "articleAdd"=>['title','author','img_url','content'],
        "articleEdit"=>['title','author','content'],
        "single"=>['content']
    ];
}