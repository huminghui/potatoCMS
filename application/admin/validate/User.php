<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/22
 * Time: 16:07
 */

namespace app\admin\validate;


use think\Validate;

class User extends Validate
{
    protected $rule=[
        'username'=>'require|unique:User',
        'password'=>'require',
    ];
    protected $message=[
        'username.require' => '用户名称不能为空',
        'username.unique' => '用户名不能重复',
        'password.require' => '密码不能为空',
    ];
}