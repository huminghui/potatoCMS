<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2020/4/18
 * Time: 10:02
 */

namespace app\admin\validate;


use think\Validate;

class AuthRule extends Validate
{
    protected $rule=[
        'title'=>'require',
        'name'=>'require',
    ];
    protected $message=[
        'title.require' => '请填写权限名称',
        'name.require' => '请填写唯一标识',
    ];
}