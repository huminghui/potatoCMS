<?php
/**
 * Created by PhpStorm.
 *Author:huminghui
 * User: Administrator
 * Date: 2019/10/31
 * Time: 18:27
 */

namespace app\admin\validate;

use think\Validate;
class Column extends Validate
{
    protected $rule=[
        'column_name'=>'require',
    ];
    protected $message=[
        'column_name.require' => '请填写栏目名称',
    ];
}